var docRoot = '/momentum/html/module360/';
var docRoot = './';
var isMobile = $(window).width() < 640;
var isGallery = false;

var pinArray = {'vehicle' : {'name':'jeep', 'trims':[{'name':'trim1', 'panorama':'jeep1.jpg', 'defaultX': 0, 'defaultY':0, 'hotspots':[{id: "overview", x: 2020, y: 900, modalImage: "cargo", modalTitle:"Overview", modalDetail: "Experience an interior that is both style-savvy and intelligently designed. With versatile cabin space, advanced technology and lush materials, the Jeep<sup class='jeepSub'>&reg;</sup>  Cherokee<sup>&reg;</sup> Overland interior is an adventure in and of itself."},
    {id: "uconnect", x: 2020, y: 1180, modalImage: "uconnect", modalTitle:"UConnect", modalDetail: "The standard Uconnect<sup>&reg;</sup> 8.4 NAV multimedia centre with largest-in-class 8.4-inch touchscreen,32 features hands-free communication<sup>16</sup> with Bluetooth<sup>&reg;</sup> streaming audio, GPS naviagation, and Siri<sup>&reg;</sup> Eyes Free.<sup>53</sup>"},
    {id: "cluster", x: 1751, y: 1150, modalImage:"cluster", modalTitle:"Cluster display", modalDetail: "The full-colour, 7-inch customizable in-cluster display centre gives you important data like fuel economy, trip information and cabin temperature.  This standard feature also displays what music you’re listening to and directions from the navigation.<sup>14</sup>."},
    {id: "steeringWheel", x: 1690, y: 1280, modalImage:"steeringWheel", modalTitle:"Steering Wheel", modalDetail: "The Overland<sup>&reg;</sup> comes with a heated, leather-wrapped steering wheel made with real wood trim. It features integrated controls for audio, cruise control and hands-free communications<sup>16</sup> so you can keep your hands safely on the wheel."},
    {id: "passengerSeat", x: 2500, y: 1610, modalImage:"seats", modalTitle:"Nappa Seats", modalDetail: "The Jeep<sup class='jeepSub'>&reg;</sup> Overland<sup>&reg;</sup> surrounds you in comfort, luxury and versatility with standard Nappa leather-faced seats, including heated and ventilated front seats with memory settings."},
    {id: "sunroof", x: 2020, y: 400, modalImage:"sunroof", modalTitle:"Sun roof", modalDetail: "Open up to new experiences with the available CommandView<sup>&reg;</sup> dual-pane power sunroof. It includes a large power-sliding glass front panel, rear fixed glass panel and a power sunshade."},
    {id: "cargoSpace", x: 3930, y: 975, modalImage:"cargo", modalTitle:"Cargo Space", modalDetail: "Keep your cargo organized with the Jeep<sup class='jeepSub'>&reg;</sup> Cargo Management System. Standard on the Overland<sup>&reg;</sup>, the fold-flat front passenger seat, 60/40 split-folding rear seats, and 18 cubbies, pockets and bins fulfill all your lifestyle needs."},
    {id: "safety", x: 2700, y: 1275, modalImage:"cargo", modalTitle:"Safety", modalDetail: "The Overland’s safety features include 10 standard air bags,<sup>12</sup> full-length side curtain, driver and front-passenger inflatable knee blocker, front-seat-mounted side thorax/pelvic, rear-seat-mounted side pelvic and advanced multistage front air bags."}
]}, {'name':'trim2', 'panorama':'jeep2.jpg', 'defaultX': 0, 'defaultY':0,'hotspots':[{id: "overview", x: 2020, y: 900, modalImage: "cargo", modalTitle:"Overview 2", modalDetail: "Experience an interior that is both style-savvy and intelligently designed. With versatile cabin space, advanced technology and lush materials, the Jeep<sup class='jeepSub'>&reg;</sup>  Cherokee<sup>&reg;</sup> Overland interior is an adventure in and of itself."},
    {id: "uconnect", x: 2020, y: 1180, modalImage: "uconnect", modalTitle:"UConnect 2", modalDetail: "The standard Uconnect<sup>&reg;</sup> 8.4 NAV multimedia centre with largest-in-class 8.4-inch touchscreen,32 features hands-free communication<sup>16</sup> with Bluetooth<sup>&reg;</sup> streaming audio, GPS naviagation, and Siri<sup>&reg;</sup> Eyes Free.<sup>53</sup>"},
    {id: "cluster", x: 1751, y: 1150, modalImage:"cluster", modalTitle:"Cluster display 2", modalDetail: "The full-colour, 7-inch customizable in-cluster display centre gives you important data like fuel economy, trip information and cabin temperature.  This standard feature also displays what music you’re listening to and directions from the navigation.<sup>14</sup>."},
    {id: "steeringWheel", x: 1690, y: 1280, modalImage:"steeringWheel", modalTitle:"Steering Wheel 2", modalDetail: "The Overland<sup>&reg;</sup> comes with a heated, leather-wrapped steering wheel made with real wood trim. It features integrated controls for audio, cruise control and hands-free communications<sup>16</sup> so you can keep your hands safely on the wheel."},
    {id: "passengerSeat", x: 2500, y: 1610, modalImage:"seats", modalTitle:"Nappa Seats 2", modalDetail: "The Jeep<sup class='jeepSub'>&reg;</sup> Overland<sup>&reg;</sup> surrounds you in comfort, luxury and versatility with standard Nappa leather-faced seats, including heated and ventilated front seats with memory settings."},
    {id: "sunroof", x: 2020, y: 400, modalImage:"sunroof", modalTitle:"Sun roof 2", modalDetail: "Open up to new experiences with the available CommandView<sup>&reg;</sup> dual-pane power sunroof. It includes a large power-sliding glass front panel, rear fixed glass panel and a power sunshade."},
    {id: "cargoSpace", x: 3930, y: 975, modalImage:"cargo", modalTitle:"Cargo Space 2", modalDetail: "Keep your cargo organized with the Jeep<sup class='jeepSub'>&reg;</sup> Cargo Management System. Standard on the Overland<sup>&reg;</sup>, the fold-flat front passenger seat, 60/40 split-folding rear seats, and 18 cubbies, pockets and bins fulfill all your lifestyle needs."},
    {id: "safety", x: 2700, y: 1275, modalImage:"cargo", modalTitle:"Safety 2", modalDetail: "The Overland’s safety features include 10 standard air bags,<sup>12</sup> full-length side curtain, driver and front-passenger inflatable knee blocker, front-seat-mounted side thorax/pelvic, rear-seat-mounted side pelvic and advanced multistage front air bags."}
]}, {'name':'trim3', 'panorama':'jeep3.jpg', 'defaultX': 0, 'defaultY':0,'hotspots':[{id: "overview 3", x: 2020, y: 900, modalImage: "cargo", modalTitle:"Overview 3", modalDetail: "Experience an interior that is both style-savvy and intelligently designed. With versatile cabin space, advanced technology and lush materials, the Jeep<sup class='jeepSub'>&reg;</sup>  Cherokee<sup>&reg;</sup> Overland interior is an adventure in and of itself."},
    {id: "uconnect", x: 2020, y: 1180, modalImage: "uconnect", modalTitle:"UConnect 3", modalDetail: "The standard Uconnect<sup>&reg;</sup> 8.4 NAV multimedia centre with largest-in-class 8.4-inch touchscreen,32 features hands-free communication<sup>16</sup> with Bluetooth<sup>&reg;</sup> streaming audio, GPS naviagation, and Siri<sup>&reg;</sup> Eyes Free.<sup>53</sup>"},
    {id: "cluster", x: 1751, y: 1150, modalImage:"cluster", modalTitle:"Cluster display 3", modalDetail: "The full-colour, 7-inch customizable in-cluster display centre gives you important data like fuel economy, trip information and cabin temperature.  This standard feature also displays what music you’re listening to and directions from the navigation.<sup>14</sup>."},
    {id: "steeringWheel", x: 1690, y: 1280, modalImage:"steeringWheel", modalTitle:"Steering Wheel 3", modalDetail: "The Overland<sup>&reg;</sup> comes with a heated, leather-wrapped steering wheel made with real wood trim. It features integrated controls for audio, cruise control and hands-free communications<sup>16</sup> so you can keep your hands safely on the wheel."},
    {id: "passengerSeat", x: 2500, y: 1610, modalImage:"seats", modalTitle:"Nappa Seats 3", modalDetail: "The Jeep<sup class='jeepSub'>&reg;</sup> Overland<sup>&reg;</sup> surrounds you in comfort, luxury and versatility with standard Nappa leather-faced seats, including heated and ventilated front seats with memory settings."},
    {id: "sunroof", x: 2020, y: 400, modalImage:"sunroof", modalTitle:"Sun roof 3", modalDetail: "Open up to new experiences with the available CommandView<sup>&reg;</sup> dual-pane power sunroof. It includes a large power-sliding glass front panel, rear fixed glass panel and a power sunshade."},
    {id: "cargoSpace", x: 3930, y: 975, modalImage:"cargo", modalTitle:"Cargo Space 3", modalDetail: "Keep your cargo organized with the Jeep<sup class='jeepSub'>&reg;</sup> Cargo Management System. Standard on the Overland<sup>&reg;</sup>, the fold-flat front passenger seat, 60/40 split-folding rear seats, and 18 cubbies, pockets and bins fulfill all your lifestyle needs."},
    {id: "safety", x: 2700, y: 1275, modalImage:"cargo", modalTitle:"Safety 3", modalDetail: "The Overland’s safety features include 10 standard air bags,<sup>12</sup> full-length side curtain, driver and front-passenger inflatable knee blocker, front-seat-mounted side thorax/pelvic, rear-seat-mounted side pelvic and advanced multistage front air bags."}
]}]}};

function initialize360(typeOf360) {

    if (typeOf360 == 'gallery') {
        isGallery = true;
    }
    // mobile and gallery both initialize as fullScreen
    if (isMobile || isGallery){
        if (isMobile) {
            $('.pageContainer').addClass('mobile');
        }
        toggleFullscreen();
    } else {
        $('#module360').css('height', '530px');
        $('.pageContainer').css('margin-bottom', '110px');
        showPSV();
    }
}

function launchIntoFullscreen(element) {
    // iPhone doesn't support fullScreen API
    if (navigator.userAgent.indexOf('iPhone') == -1 && navigator.userAgent.indexOf('iPad') == -1) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }
}
// todo - incorporate proper toggling, exitFullScreen currently handled in close360()
toggleFullscreen = function(){
    launchIntoFullscreen(document.getElementById("module360"));
    $('#module360').css('height', '100%').css('width', '100%').css('position', 'fixed').css('z-index', '99');
    $('.pageContainer').css('margin-bottom', '110px');
    showPSV();
};

function showPSV() {
    // Listen for fullscreen open/exit - true=exiting from fullScreen
    $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e)
    {
        if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement == true) {
        } else if (isMobile || isGallery) {
            $('#module360').empty();
            $('.pageContainer').css('margin-bottom', '0px').css('position', '');
            $('#module360').css('height','0px' );
        }
    });

    var PSV = new PhotoSphereViewer({
        panorama: docRoot + '360Images/'+pinArray['vehicle']['trims'][0]['panorama'],
        container: 'module360',
        loading_img: docRoot+'images/photosphere-logo.gif',
        autoRotate: false,
        gyroscope: true,
        navbar: [
            'zoom',
            {
                title: 'Reset View',
                className: 'custom-button-reset-view',
                content: '<img src="'+docRoot+'images/360_icon_reset.svg">',
                onClick: (function () {
                    return function () {
                        PSV.animate({
                            longitude: 0,
                            latitude: 0
                        }, 1000)
                    }
                }())
            },
            'autorotate',
            {
                title: 'Pause Rotation',
                className: 'custom-button-pause-rotation',
                content: '<img src="'+docRoot+'images/360_icon_pause.svg">',
                onClick: (function () {
                    return function () {
                        PSV.stopAutorotate();
                    }
                }())
            },
            'fullscreen'

        ],
        markers: (function () {
            var a = [];
            for (var i = -1; i++, i <= pinArray['vehicle']['trims'][0]['hotspots'].length - 1;) {
                a.push({
                    "id": pinArray['vehicle']['trims'][0]['hotspots'][i].id,
                    "name": pinArray['vehicle']['trims'][0]['hotspots'][i].name,
                    "x": pinArray['vehicle']['trims'][0]['hotspots'][i].x,
                    "y": pinArray['vehicle']['trims'][0]['hotspots'][i].y,
                    "width": 15,
                    "height": 15,
                    "image": docRoot + 'images/fca_pulse_empty.gif'
                })
            }
            return a;
        }()),
        lang: {
            autorotate: 'Automatic rotation',
            zoom: 'Zoom Slider',
            zoomOut: 'Zoom out',
            zoomIn: 'Zoom in',
            download: 'Download',
            fullscreen: 'Fullscreen',
            markers: 'Markers',
            gyroscope: 'Gyroscope'
        }
    });

    // Do some swapping of visibility for play vs. pause states
    PSV.on('autorotate', function(enabled) {
        if (enabled){
            $('.psv-autorotate-button').hide();
            $('.custom-button-pause-rotation').show();
        } else {
            $('.psv-autorotate-button').show();
            $('.custom-button-pause-rotation').hide();
        }
    });

    // Hover effect for SVG buttons
    $('.psv-button-svg').on('mouseover', function(){
        $(this).find('.st0').css('fill', 'rgba(255, 255, 255, 1)');
    }).on('mouseout', function(){
        $(this).find('.st0').css('fill', 'rgba(255, 255, 255, .7)');
    });

    PSV.on('select-marker', function (marker) {
        showModal(marker.id);
    });

    PSV.on('ready', function () {
        $('#module360').show();

        if (isMobile) {
            $('.psv-navbar')
                .css('background-color', '#fff')
                .css('opacity', '1')
                .css('height', '45px')
                .css('padding-top', '5px')
                .css('bottom', '0px');
        }
        if (isGallery) {
            $('.psv-navbar')
                .css('background-color', '#fff')
                .css('opacity', '1')
                .css('height', '50px')
                .css('padding', '5px')
                .css('bottom', '15px');
        }
        $('.mobileFullscreenClose').fadeIn(500);
    });

    // Reposition NavBar before/after fullscreenToggle
    PSV.on('fullscreen-updated', function(enabled) {
        if (!isMobile) {
            if (enabled) {
                $('.psv-navbar').css('bottom', '0px');
                $('.psv-navbar').css('background-color', '#fff');
                $('.psv-navbar').css('height', '53px');
                $('.psv-navbar').css('padding-top', '3px');
                $('.psv-navbar').css('opacity', '0.9');
                $('.psv-navbar').css('padding-left', '3px');
                $('.psv-fullscreen-button').css('margin-right', '6px');
                $('.psv-canvas-container canvas').css('border', '0px').css('border-radius', '0px');
                $('.psv-navbar').show();
            } else {
                close360Modals();
                $('.psv-navbar').removeAttr('style');
                $('.psv-fullscreen-button').removeAttr('style');
                $('.psv-navbar').css('bottom', '-60px');
                $('.psv-container').removeAttr('style');
                $('.psv-navbar').show();
            }
        }
    });
    PSV.on('panorama-load-progress', function(panorama, progress) {
        // Possible loading animation here
    });

    // Rotate to marker and position popup relative to it
    showModal = function(modalId){
        $('.module360Modal').fadeOut(500);
        PSV.gotoMarker(modalId, 750);

        $('.modalBackground').hide(500);
        $('.psv-canvas-container').removeClass('blur');
        $('.psv-marker').removeClass('blur');

        $('.modalBackground').show(500);

        if ($(window).width() < 640) {
            $('.module360Modals').css('left', ($('.psv-hud').width() - $('.module360Modals').width()) / 2 + 'px');
        } else {
            $('.module360Modals').css('left', ($('.psv-hud').width() - $('.module360Modals').width()) / 2 + 'px');
            $('.module360Modals').css('top', ($('.psv-hud').height() - $('.module360Modals').height()) / 2 + 'px');
        }
        setTimeout(function () {
            $('.modalBackground').show();
            $('.psv-canvas-container').addClass('blur');
            $('.psv-marker').addClass('blur');
            $('#' + modalId).fadeIn(500);
            $('.psv-navbar').addClass('navbarBlur');
        }, 500);
    };

    // Close option for mobile.  Will revert to 'Click Here' display
    close360 = function(){
        // iPhone doesn't support fullScreen API
        if (navigator.userAgent.indexOf('iPhone') > -1 || navigator.userAgent.indexOf('iPad') > -1) {
            $('#module360').empty();
            $('.pageContainer').css('margin-bottom', '0px').css('position', '');
            $('#module360').css('height','0px' );
        } else {
            (document.exitFullscreen || document.mozCancelFullScreen || document.webkitExitFullscreen || document.msExitFullscreen).call(document);
        }
    };
    close360Modals = function () {
        $('.modalBackground').hide(500);
        $('.psv-canvas-container').removeClass('blur');
        $('.psv-marker').removeClass('blur');
        $('.module360Modal').fadeOut(500);
        $('.psv-navbar').removeClass('navbarBlur');
    };

    // Pre-populate modal collection for SEO purposes
    var modalHTML =  '<div class="modalBackground" onclick="close360Modals()"></div>';
    modalHTML += '<div class="module360Modals">';
    for (var i = 0; i <= pinArray['vehicle']['trims'][0]['hotspots'].length - 1; i++) {
        modalHTML +=
            '<div id ="' + pinArray['vehicle']['trims'][0]['hotspots'][i].id + '" class="module360Modal" style="display:none">' +
            '<div class="modalClose outer" onclick="close360Modals();">'+
            '<div class="inner">' +
            '<label>Back</label>' +
            '</div>' +
            '</div>' +
            '<div class="modalClose mobile" onclick="close360Modals();"></div>' +
            '<div class="modalImage">' +
            '<img src="' + docRoot + 'images/' + pinArray['vehicle']['trims'][0]['hotspots'][i].modalImage + '.jpg" width="100%"  />' +
            '</div>' +
            '<div class="modalCopy">' +
            '<span class="modalDetail">' + pinArray['vehicle']['trims'][0]['hotspots'][i].modalDetail + '</span>' +
            '</div>' +
            '<div class="modalFooter">';
        if (i > 0) {
            modalHTML += '<span class="modalFooterLeft" onclick="showModal(\''+pinArray['vehicle']['trims'][0]['hotspots'][i-1].name + '\')"><span class="modalFooterLeftArrow"></span>' + pinArray['vehicle']['trims'][0]['hotspots'][i-1].modalTitle + '</span>'
        } else {
            modalHTML += '<span class="modalFooterLeft" onclick="showModal(\''+pinArray['vehicle']['trims'][0]['hotspots'][pinArray['vehicle']['trims'][0]['hotspots'].length-1].modalTitle + '\')"><span class="modalFooterLeftArrow"></span>' + pinArray['vehicle']['trims'][0]['hotspots'][pinArray['vehicle']['trims'][0]['hotspots'].length-1].modalTitle + '</span>'
        }
        if (i != pinArray['vehicle']['trims'][0]['hotspots'].length-1) {
            modalHTML += '<span class="modalFooterRight" onclick="showModal(\''+pinArray['vehicle']['trims'][0]['hotspots'][i+1].id + '\')">' + pinArray['vehicle']['trims'][0]['hotspots'][i+1].modalTitle + '<span class="modalFooterRightArrow"></span></span>'
        } else {
            modalHTML += '<span class="modalFooterRight" onclick="showModal(\''+pinArray['vehicle']['trims'][0]['hotspots'][0].id + '\')">' + pinArray['vehicle']['trims'][0]['hotspots'][0].modalTitle + '<span class="modalFooterRightArrow"></span></span>'
        }
        modalHTML += '</div>' +
            '</div>'
    }
    modalHTML += '</div>';

    // Insert Close button into element for fullscreen-mobile closing
    if (isMobile) {
        modalHTML += "<div class='mobileTopnav' onclick='close360()'>Model Shown:  2017 Jeep<sub>&reg;</sub> Cherokee";
        modalHTML += "<span class='mobileTopnavClose'>X</span>";
        modalHTML += "</div>";
    }

    // Drop down for trim-level selection
    modalHTML += '<select id="soflow" onchange="select360Trim(this)">'+
    '<option>Select an Trim</option>';
    for (var i=0;i<=pinArray['vehicle']['trims'].length-1; i++) {
        modalHTML += '<option value="'+i+'">'+pinArray['vehicle']['trims'][i].name+'</option>';
    }
    modalHTML += '</select>';

    // Update 360 with selected version from dropdown
    select360Trim = function(e) {
        PSV.setPanorama(
            docRoot + '/360Images/'+pinArray['vehicle']['trims'][e.value].panorama,
            {
                longitude: pinArray['vehicle']['trims'][e.value]['defaultX'],
                latitude: pinArray['vehicle']['trims'][e.value]['defaultY']
            },
            true
        );
        PSV.clearMarkers();
        for (var i = -1; i++, i <= pinArray['vehicle']['trims'][e.value]['hotspots'].length - 1;) {
            PSV.addMarker({
                "id": pinArray['vehicle']['trims'][e.value]['hotspots'][i].id,
                "name": pinArray['vehicle']['trims'][e.value]['hotspots'][i].name,
                "x": pinArray['vehicle']['trims'][e.value]['hotspots'][i].x,
                "y": pinArray['vehicle']['trims'][e.value]['hotspots'][i].y,
                "width": 15,
                "height": 15,
                "image": docRoot + 'images/fca_pulse_empty.gif'
            })
        }
        $('.module360Modal').remove();
        modalHTML = '';
        for (var i = 0; i <= pinArray['vehicle']['trims'][e.value]['hotspots'].length - 1; i++) {
            modalHTML +=
                '<div id ="' + pinArray['vehicle']['trims'][e.value]['hotspots'][i].id + '" class="module360Modal" style="display:none">' +
                '<div class="modalClose outer" onclick="close360Modals();">'+
                '<div class="inner">' +
                '<label>Back</label>' +
                '</div>' +
                '</div>' +
                '<div class="modalClose mobile" onclick="close360Modals();"></div>' +
                '<div class="modalImage">' +
                '<img src="' + docRoot + 'images/' + pinArray['vehicle']['trims'][e.value]['hotspots'][i].modalImage + '.jpg" width="100%"  />' +
                '</div>' +
                '<div class="modalCopy">' +
                '<span class="modalDetail">' + pinArray['vehicle']['trims'][e.value]['hotspots'][i].modalDetail + '</span>' +
                '</div>' +
                '<div class="modalFooter">';
            if (i > 0) {
                modalHTML += '<span class="modalFooterLeft" onclick="showModal(\''+pinArray['vehicle']['trims'][e.value]['hotspots'][i-1].name + '\')"><span class="modalFooterLeftArrow"></span>' + pinArray['vehicle']['trims'][e.value]['hotspots'][i-1].modalTitle + '</span>'
            } else {
                modalHTML += '<span class="modalFooterLeft" onclick="showModal(\''+pinArray['vehicle']['trims'][e.value]['hotspots'][pinArray['vehicle']['trims'][e.value]['hotspots'].length-1].modalTitle + '\')"><span class="modalFooterLeftArrow"></span>' + pinArray['vehicle']['trims'][e.value]['hotspots'][pinArray['vehicle']['trims'][e.value]['hotspots'].length-1].modalTitle + '</span>'
            }
            if (i != pinArray['vehicle']['trims'][e.value]['hotspots'].length-1) {
                modalHTML += '<span class="modalFooterRight" onclick="showModal(\''+pinArray['vehicle']['trims'][e.value]['hotspots'][i+1].id + '\')">' + pinArray['vehicle']['trims'][e.value]['hotspots'][i+1].modalTitle + '<span class="modalFooterRightArrow"></span></span>'
            } else {
                modalHTML += '<span class="modalFooterRight" onclick="showModal(\''+pinArray['vehicle']['trims'][e.value]['hotspots'][i].id + '\')">' + pinArray['vehicle']['trims'][e.value]['hotspots'][i].modalTitle + '<span class="modalFooterRightArrow"></span></span>'
            }
            modalHTML += '</div>' +
            '</div>';
        }
        $('.module360Modals').append(modalHTML);
    };

    // Insert into 360 container to account for positioning, z-index, etc.
    $('.psv-container').append(modalHTML);

    // Insert trim dropdown

    $('.modalFooterRight')
        .on('mouseover', function(){
            $('.modalFooterRightArrow').animate({'left':'5px'}, 'fast')
        })
        .on('mouseout', function(){
            $('.modalFooterRightArrow').animate({'left':'0px'}, 'fast')
        });
    $('.modalFooterLeft')
        .on('mouseover', function(){
            $('.modalFooterLeftArrow').animate({'left':'-5px'}, 'fast')
        })
        .on('mouseout', function(){
            $('.modalFooterLeftArrow').animate({'left':'0px'}, 'fast')
        });
}
